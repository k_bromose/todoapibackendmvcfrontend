﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace ToDo.Mvc.Proxy
{
    public interface IApiProxy<T> where T : class
    {
        Task<IReadOnlyList<T>> GetAll();

        Task<T> Get(int id);

        Task Create(T item);

        Task Update(int id, T item);

        Task Delete(int id);
        Task<bool> Exists(int id);
    }

    public class ApiProxy<T> : IApiProxy<T> where T : class
    {
        private readonly HttpClient _client;

        public ApiProxy(HttpClient client)
        {
            _client = client;
        }


        async Task IApiProxy<T>.Create(T item)
        {
            throw new NotImplementedException();
        }

        async Task IApiProxy<T>.Delete(int id)
        {
            throw new NotImplementedException();
        }

        async Task<bool> IApiProxy<T>.Exists(int id)
        {
            throw new NotImplementedException();
        }

        async Task<T> IApiProxy<T>.Get(int id)
        {
            throw new NotImplementedException();
        }

        Task<IReadOnlyList<T>> IApiProxy<T>.GetAll()
        {
            var res = _client.GetAsync(new Uri(_client.BaseAddress, "Api/ToDoItem")).Result;
            if (res.IsSuccessStatusCode)
            {
                return res.Content.ReadAsAsync<IReadOnlyList<T>>();
            }

            throw new Exception($"Unsuccesful API call: {res.ReasonPhrase}");
        }

        async Task IApiProxy<T>.Update(int id, T item)
        {
            throw new NotImplementedException();
        }
    }
}
