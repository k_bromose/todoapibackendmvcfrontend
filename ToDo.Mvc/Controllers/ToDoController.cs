﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ToDo.Mvc.Models;
using ToDo.Mvc.Proxy;

namespace ToDo.Mvc.Controllers
{
    public class ToDoController : Controller
    {
        private readonly IApiProxy<ToDoItem> _service;

        public ToDoController(IApiProxy<ToDoItem> service)
        {
            _service = service;
        }

        // GET: ToDoItem
        public async Task<IActionResult> Index()
        {
            return View(await _service.GetAll());
        }

        // GET: ToDoItem/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toDoItem = await _service.Get(id.Value);
            if (toDoItem == null)
            {
                return NotFound();
            }

            return View(toDoItem);
        }

        // GET: ToDoItem/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ToDoItem/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,IsComplete")] ToDoItem toDoItem)
        {
            if (ModelState.IsValid)
            {
                await _service.Create(toDoItem);
                return RedirectToAction(nameof(Index));
            }
            return View(toDoItem);
        }

        // GET: ToDoItem/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toDoItem = await _service.Get(id.Value);
            if (toDoItem == null)
            {
                return NotFound();
            }
            return View(toDoItem);
        }

        // POST: ToDoItem/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,IsComplete")] ToDoItem toDoItem)
        {
            if (id != toDoItem.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _service.Update(id, toDoItem);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await ToDoItemExists(toDoItem.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(toDoItem);
        }

        // GET: ToDoItem/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toDoItem = await _service.Get(id.Value);
            if (toDoItem == null)
            {
                return NotFound();
            }

            return View(toDoItem);
        }

        // POST: ToDoItem/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _service.Delete(id);
            //    .ToDoItem.FindAsync(id);
            //_context.ToDoItem.Remove(toDoItem);
            //await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private Task<bool> ToDoItemExists(int id)
        {
            return _service.Exists(id);
        }
    }
}
